﻿using Pvf.Core;
using Pvf.Core.Contents;
using Pvf.Core.Tests;
using Pvf.Runtime.Equipment;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvf.Parser
{
    /// <summary>
    /// 
    /// </summary>
    public class FEquipment : FObject
    {

    }

    public class FImgSource : FObject
    {
        //public FImgSource(IPvfEnv env, IPvfModule module) : base(env, module)
        //{
        //}
    }

    public class FEquipmentType : FObject
    {
        //public FEquipmentType(IPvfEnv env, IPvfModule module) : base(env, module)
        //{
        //}
    }

    class Program
    {
        static void Init(ScriptVM vm)
        {
            var imgFieldMeta = new FObjectMeta()
            {
                FieldType = "FImgSource",
                ObjType = typeof(FImgSource),
                FieldsIsRequired = true
            };
            imgFieldMeta.Register("", new FRefStringMeta());
            imgFieldMeta.Register("", new FInt32Meta());
            vm.AddMetaSource(imgFieldMeta);

            var equipmentTypeMeta = new FObjectMeta()
            {
                FieldType = "FEquipmentType",
                ObjType = typeof(FEquipmentType),
                FieldsIsRequired = true
            };
            equipmentTypeMeta.Register("", new FRefStringMeta());
            equipmentTypeMeta.Register("", new FInt32Meta());
            vm.AddMetaSource(equipmentTypeMeta);

            var vector2Meta = new FObjectMeta()
            {
                FieldType = "FVector2",
                FieldsIsRequired = true
            };
            vector2Meta.Register("", new FInt32Meta(), MetaRegisterType.Array);
            vm.AddMetaSource(vector2Meta);

            vm.AddMetaSource(EnumSource.Create("rarity", EnumItem.Create(0, "common")));
            vm.AddMetaSource(EnumSource.Create("usable job", EnumItem.Create(0, "at fighter")));
            vm.AddMetaSource(EnumSource.Create("attach type", EnumItem.Create(0, "trade")));

            var equipmentMeta = new FObjectMeta()
            {
                FieldType = "FEquipment",
                ObjType = typeof(FEquipment)
            };
            vm.AddMetaSource(equipmentMeta);

            equipmentMeta.Register("name", new FNameMeta())
                         .Register("name2", new FNameMeta())
                         .Register("grade", new FInt32Meta())
                         .Register("rarity", new FEnumInt32Meta() { UnderingType = "rarity" })
                         .Register("usable job", new FEnumStringMeta() { UnderingType = "usable job" }, MetaRegisterType.List)
                         .Register("attach type", new FEnumStringMeta() { UnderingType = "attach type" })
                         .Register("minimum level", new FInt32Meta())
                         .Register("price", new FInt32Meta())
                         .Register("repair price", new FInt32Meta())
                         .Register("value", new FInt32Meta())
                         .Register("equipment physical defense", vm.GetMetaSource<FObjectMeta>("vector2"))
                         .Register("equipment type", vm.GetMetaSource<FObjectMeta>("equipment type"));
        }

        static async Task Main(string[] args)
        {
            //注册编码提供程序
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            PvfEnv.StringTableBinOutputFile = "d:\\string_table.bin.txt";
            PvfEnv.IsDevelopmentMode = true;

            using var env = new PvfEnv(@"f:\DOFData\Script.pvf", "zh-cn", "kor",
                logger: new DefaultConsoleLogger());

            if(!await env.LoadAsync())
            {
                return;
            }

            Init(env.VM);

            Console.WriteLine("1-----------------------------------------------");
            env.Configure(typeof(Equipment).Assembly);
            Console.WriteLine("2-----------------------------------------------");

            FieldCatcher.Begin();

            var equMod = env.GetModule<EquipmentModule>();

            Console.WriteLine($"-----------------------------------------------");
            var sw = new Stopwatch();

            sw.Restart();

            int equId = equMod.Objects.Keys.FirstOrDefault();

            //!!!暂时未实现请不要取消注释
            //var script = equMod.GetObject<FEquipment>(equId);
            //Console.WriteLine(script);

            var txtScript = equMod.GetItem(equId).GetRef().GetContent<PvfFormattedStringContent>().Data;
            Console.WriteLine(txtScript);
            System.IO.File.WriteAllText($"d:\\equipment_{equId}.equ.txt", txtScript);

            //!!!下面这一段加载所有的装备脚本
            //List<Equipment> lst = new List<Equipment>();

            //foreach (var obj in equMod.Objects)
            //{
            //    var item = equMod.GetItem(obj.Key);

            //    try
            //    {
            //        var equ = item.GetObjectContent<Equipment>();
            //        if (equ == null)
            //        {
            //            Console.WriteLine(item.Index + "\r\n" + item.File);
            //        }
            //        lst.Add(equ);
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine($"{obj.Key}\r\n{item.GetRef().GetContent<PvfFormattedStringContent>().Data}\r\n{ex.Message}");
            //        break;
            //    }
            //}

            sw.Stop();
            Console.WriteLine($"Split->>>>>{sw.ElapsedMilliseconds}");


            FieldCatcher.Clear("d:\\equipment_unused_fields.txt");

            Console.Read();
        }
    }
}
