﻿using System;
using System.Collections.Generic;

namespace Pvf.Core
{
    /// <summary>
    /// 动态转换器
    /// </summary>
    public interface IDynamicElementConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Type GetBaseType();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Type GetEnumType();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        IDynamicElementEnumConverterData GetSubConverterData(int key);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="converterData"></param>
        void SetEntityKey(ref IDynamicElementEntity entity, IDynamicElementEnumConverterData converterData);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TBase"></typeparam>
    public interface IDynamicElementCollectionConverter<TBase> : IDynamicElementConverter
    {
        ICollection<TBase> CreateEmpty();
    }
}
