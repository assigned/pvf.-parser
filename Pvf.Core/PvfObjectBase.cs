﻿namespace Pvf.Core
{
    /// <summary>
    /// Pvf对象
    /// </summary>
    public abstract class PvfObjectBase
    {
        /// <summary>
        /// 对象ID
        /// </summary>
        [PvfIngore]
        public int ObjectId { get; set; }

        public override string ToString()
        {
            return ObjectId.ToString();
        }
    }
}