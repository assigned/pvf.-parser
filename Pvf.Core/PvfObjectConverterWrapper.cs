﻿using System;
using System.IO;

namespace Pvf.Core
{
    internal abstract class PvfObjectConverterWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        public IPvfConverter Converter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Action<object, object> Setter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Func<object, object> Getter { get; set; }
    }

    internal abstract class PvfObjectConverterWrapper<TSrc> : PvfObjectConverterWrapper
    {
        public abstract void SetValue(BinaryReader reader, TSrc src, IPvfModule module);
    }

    internal sealed class PvfObjectConverterWrapper<TSrc, TProperty> : PvfObjectConverterWrapper<TSrc>
    {
        public override void SetValue(BinaryReader reader, TSrc src, IPvfModule module)
        {
            if (reader.IsEndOfStream())
            {
                return;
            }

            if (!(Converter is IBinaryPvfConverter<TProperty> binaryPvfConverter)) return;
            var value = binaryPvfConverter.Deserialize(reader, module);
            Setter(src, value);
        }
    }
}
