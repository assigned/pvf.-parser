﻿namespace Pvf.Core.Types
{
    /// <summary>
    /// 二维向量
    /// </summary>
    public struct MulVal2
    {
        public float X { get; set; }

        public float Y { get; set; }
    }

    /// <summary>
    /// 三维向量
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct MulVal3
    {
        public float X { get; set; }

        public float Y { get; set; }

        public float Z { get; set; }
    }

    /// <summary>
    /// 四维向量
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct MulVal4
    {
        public float X { get; set; }

        public float Y { get; set; }

        public float Z { get; set; }

        public float W { get; set; }
    }
}
