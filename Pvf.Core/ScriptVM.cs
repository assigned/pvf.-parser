using System;
using System.Collections.Generic;

namespace Pvf.Core
{
    public interface IScriptType
    {
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        string UniqueName { get; }
    }

    /// <summary>
    /// 脚本虚拟环境
    /// </summary>
    public class ScriptVM
    {
        private readonly Dictionary<string, IScriptType> _metaSource = new Dictionary<string, IScriptType>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        public void AddMetaSource(IScriptType src)
        {
            _metaSource.Add(src.UniqueName, src);
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <typeparam name="TMetaType"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public TMetaType GetMetaSource<TMetaType>(string key)
        where TMetaType : class, IScriptType
        {
            if (_metaSource.TryGetValue(key, out var type))
            {
                return type as TMetaType;
            }
            return null;
        }

    }
}
