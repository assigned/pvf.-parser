﻿namespace Pvf.Core
{
    /// <summary>
    /// 可初始化的PVF模块
    /// </summary>
    public interface IPvfModuleInitializer
    {
        /// <summary>
        /// 初始化函数
        /// </summary>
        void Init();
    }
}
