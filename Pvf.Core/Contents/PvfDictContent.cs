﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Pvf.Core.Contents
{
    public sealed class PvfDictContent : PvfContent<Dictionary<string, string>>
    {
        private Dictionary<string, string> _stringData;

        public override Dictionary<string, string> Data => _stringData;

        public override bool Load(Encoding encoding, IPvfModule module)
        {
            if (_stringData != null)
            {
                return true;
            }
            if (!base.Load(encoding, module))
            {
                return false;
            }
            _stringData = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            using (var ms = new MemoryStream(BytesData))
            {
                using (var reader = new StreamReader(ms, encoding))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (!string.IsNullOrEmpty(line))
                        {
                            var split = line.Split('>');
                            if (split.Length == 2)
                            {
                                _stringData[split[0]] = split[1];
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}
