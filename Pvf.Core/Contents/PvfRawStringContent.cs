﻿using System.Text;

namespace Pvf.Core.Contents
{
    public sealed class PvfRawStringContent : PvfContent<string>
    {
        private string _stringData;

        public override string Data => _stringData;

        public override bool Load(Encoding encoding, IPvfModule module)
        {
            if (!string.IsNullOrEmpty(_stringData))
            {
                return true;
            }
            if (!base.Load(encoding, module))
            {
                return false;
            }
            _stringData = encoding.GetString(BytesData).TrimEnd(new char[1]);
            return true;
        }
    }
}
