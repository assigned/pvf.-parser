﻿namespace Pvf.Core
{
    /// <summary>
    /// Pvf索引引用
    /// </summary>
    public sealed class PvfIndexRef
    {
        /// <summary>
        /// 
        /// </summary>
        public string File { get; internal set; }

        /// <summary>
        /// 
        /// </summary>
        public IPvfEnv Env { get; internal set; }


        /// <summary>
        /// 
        /// </summary>
        public IPvfModule Module { get; internal set; }

        /// <summary>
        /// 
        /// </summary>
        public int Index { get; internal set; }


        private PvfIndex _ref;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PvfIndex GetRef()
        {
            if (_ref != null)
            {
                return _ref;
            }

            var file = Module.CombinePath(File);

            _ref = Env.FindIndex(file);

            return _ref;
        }

        public override string ToString()
        {
            return File;
        }
    }
}
