﻿using System.IO;

namespace Pvf.Core.Converters
{
    internal sealed class PvfSingleConverter : PvfBinaryConverter<float>
    {
        public override bool CanRead(BinaryReader reader)
        {
            var peek = reader.PeekChar();
            return peek == 4 || peek == 2;
        }

        public override float Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            var c = reader.PeekChar();
            TypeCheck(reader, 4, 2);

            if(c == 2)
            {
                return reader.ReadInt32();
            }
            else
            {
                return reader.ReadSingle();
            }
        }
    }
}
