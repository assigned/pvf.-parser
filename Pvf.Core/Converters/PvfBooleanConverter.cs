﻿using System.IO;

namespace Pvf.Core.Converters
{
    internal sealed class PvfBooleanConverter : PvfBinaryConverter<bool>
    {
        public override bool CanRead(BinaryReader reader)
        {
            var peek = reader.PeekChar();
            return peek == 2;
        }

        public override bool Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            if (Layout.IsEmpty)
            {
                return true;
            }

            TypeCheck(reader, 2);

            return 1 == reader.ReadInt32();
        }
    }
}
