﻿using Pvf.Core.Types;
using System.IO;

namespace Pvf.Core.Converters
{
    internal sealed class MulVal2Converter : PvfBinaryConverter<MulVal2>
    {
        public override MulVal2 Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            MulVal2 val2 = new MulVal2
            {
                X = reader.ReadSingle(),
                Y = reader.ReadSingle()
            };

            return val2;
        }
    }

    internal sealed class MulVal3Converter : PvfBinaryConverter<MulVal3>
    {
        public override MulVal3 Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            MulVal3 val3 = new MulVal3
            {
                X = reader.ReadSingle(),
                Y = reader.ReadSingle(),
                Z = reader.ReadSingle()
            };

            return val3;
        }
    }

    internal sealed class MulVal4Converter : PvfBinaryConverter<MulVal4>
    {
        public override MulVal4 Deserialize(BinaryReader reader, IPvfModule module = null)
        {
            MulVal4 val4 = new MulVal4
            {
                X = reader.ReadSingle(),
                Y = reader.ReadSingle(),
                Z = reader.ReadSingle(),
                W = reader.ReadSingle()
            };

            return val4;
        }
    }
}
