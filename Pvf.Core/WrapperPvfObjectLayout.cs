﻿using System;

namespace Pvf.Core
{
    internal sealed class WrapperPvfObjectLayout : PvfObjectLayout
    {
        public WrapperPvfObjectLayout(Type objType, IPvfEnv env) : base(objType, env)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public PvfObjectConverterWrapper ConverterWrapper { get; set; }
    }
}
