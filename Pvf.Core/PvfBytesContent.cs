﻿namespace Pvf.Core
{
    public sealed class PvfBytesContent : PvfContent<byte[]>
    {
        public override byte[] Data => BytesData;
    }
}
