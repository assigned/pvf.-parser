﻿using System.IO;

namespace Pvf.Core
{
    public static class BinaryReaderExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static bool IsEndOfStream(this BinaryReader reader)
        {
            return reader.BaseStream.Position == reader.BaseStream.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static bool UnitNextPack(this BinaryReader reader)
        {
            return UnitNextPack(reader, out _);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool UnitNextPack(this BinaryReader reader, out int type)
        {
            type = reader.PeekChar();
            if (type == -1)
            {
                return false;
            }

            if (type == 0)
            {
                return true;
            }

            if (type != 2 && type != 4 && type != 5 && type != 6 && type != 7 && type != 8 && type != 10)
            {
                if (reader.BaseStream.Position + 5 <= reader.BaseStream.Length)
                {
                    reader.BaseStream.Position += 5;
                }
            }

            return true;
        }
    }
}
