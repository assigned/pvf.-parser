﻿using System;
using System.Collections.Generic;

namespace Pvf.Core
{
    /// <summary>
    /// Pvf环境
    /// </summary>
    public interface IPvfEnv : IDisposable
    {
        /// <summary>
        /// UUID
        /// </summary>
        string UUID { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        int Version { get; set; }

        /// <summary>
        /// 索引加密向量
        /// </summary>
        uint IndexVector { get; set; }

        /// <summary>
        /// NString加密向量
        /// </summary>
        uint NStringEntryKey { get; set; }

        /// <summary>
        /// 文件浏览
        /// </summary>
        IFileExplorer FileExplorer { get; }

        /// <summary>
        /// 日志记录
        /// </summary>
        ILogger Logger { get; }

        /// <summary>
        /// 语言
        /// </summary>
        string Lang { get; set; }

        /// <summary>
        /// str 本地化
        /// </summary>
        string NLang { get; set; }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        PvfIndex FindIndex(string fileName);

        /// <summary>
        /// 解密数据
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="len"></param>
        /// <param name="crc32"></param>
        /// <returns></returns>
        byte[] UnpackContent(int pos, int len, uint crc32);

        /// <summary>
        /// 
        /// </summary>
        IDictionary<int, string> StringTable { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<string, HashSet<int>> ReverseStringTable { get; }

        /// <summary>
        /// 
        /// </summary>
        IDictionary<string, string> NStringTable { get; }

        /// <summary>
        /// 获取Pvf模块
        /// </summary>
        /// <returns></returns>
        TModule GetModule<TModule>() where TModule : class, IPvfModule;

        /// <summary>
        /// 配置对象和模块
        /// </summary>
        /// <param name="assembly"></param>
        void Configure(System.Reflection.Assembly assembly);

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        ScriptVM VM {get;}
    }
}
