﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Pvf.Core
{
    public interface IPvfObjectLayoutCreator
    {
        IPvfObjectLayout CreateLayoutTemplate();
    }

    public interface IPvfObjectLayout
    {
        /// <summary>
        /// 
        /// </summary>
        IPvfEnv Env { get; }

        /// <summary>
        /// 
        /// </summary>
        Type ObjType { get; }

        /// <summary>
        /// 
        /// </summary>
        PropertyInfo BaseProperty { get; }

        /// <summary>
        /// 
        /// </summary>
        List<int> Keys { get; }

        /// <summary>
        /// 
        /// </summary>
        List<int> ClosedKeys { get; }

        /// <summary>
        /// 一般适用于对象中的class或者struct属性 如果为true 将会按顺序反序列化
        /// </summary>
        bool IsRequired { get; set; }

        /// <summary>
        /// 一般适用于bool字段 如果为true 不会读取数值 ,而是直接设置为true
        /// </summary>
        bool IsEmpty { get; set; }

        /// <summary>
        /// 一般适用于
        /// </summary>
        bool IsDynamic { get; set; }
    }
}
