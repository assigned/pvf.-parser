﻿namespace Pvf.Core
{
    /// <summary>
    /// 日志记录
    /// </summary>
    public interface ILogger
    {
        void LogError(string type, string msg);

        void LogInfo(string type, string msg);

        void LogWarning(string type, string msg);

        void LogException(string type, System.Exception exception);
    }
}
