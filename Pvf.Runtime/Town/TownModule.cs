﻿using Pvf.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pvf.Runtime.Town
{
    /// <summary>
    /// 
    /// </summary>
    [ModuleEntry("town")]
    public sealed class TownModule : PvfModule
    {
        public TownModule(IPvfEnv env, string name, Encoding encoding) : base(env, name, encoding)
        {
        }
    }
}
