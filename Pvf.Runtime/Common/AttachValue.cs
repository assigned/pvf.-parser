﻿using Pvf.Core;

namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 属性值
    /// </summary>
    [PvfRequired]
    public class AttachValue : IAttachValue
    {
        [PvfIngore]
        public AttributeDefine Key { get; set; }

        public float Value { get; set; }

        public override string ToString()
        {
            return $"{Key}::{Value:0.00}";
        }
    }

}
