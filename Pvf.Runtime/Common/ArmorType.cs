﻿namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 防具类型
    /// </summary>
    public enum ArmorType
    {
        /// <summary>
        /// 布甲
        /// </summary>
        Cloth = 0,

        /// <summary>
        /// 重甲
        /// </summary>
        HArmor = 1,

        /// <summary>
        /// 轻甲
        /// </summary>
        LArmor = 2,

        /// <summary>
        /// 皮甲
        /// </summary>
        Leather = 3,

        /// <summary>
        /// 板甲
        /// </summary>
        Plate = 4
    }

}
