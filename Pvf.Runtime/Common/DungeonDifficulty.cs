﻿namespace Pvf.Runtime.Common
{
    public enum DungeonDifficulty
    {
        /// <summary>
        /// 普通级
        /// </summary>
        Easy,
        /// <summary>
        /// 冒险级
        /// </summary>
        Hard,
        /// <summary>
        /// 勇士级
        /// </summary>
        Medium,
        /// <summary>
        /// 王者级
        /// </summary>
        Ultimate,
        /// <summary>
        /// 噩梦级
        /// </summary>
        Hero
    }

}
