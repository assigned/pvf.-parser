﻿namespace Pvf.Runtime.Common
{
    public enum CharacterClass
    {
        /// <summary>
        /// 全部职业
        /// </summary>
        All = -2,
        /// <summary>
        /// 
        /// </summary>
        None = -1,
        /// <summary>
        /// 鬼剑士
        /// </summary>
        Swordman,
        /// <summary>
        /// 格斗家
        /// </summary>
        Fighter,
        /// <summary>
        /// 神枪手
        /// </summary>
        Gunner,
        /// <summary>
        /// 魔法师
        /// </summary>
        Mage,
        /// <summary>
        /// 圣职者
        /// </summary>
        Priest,
        /// <summary>
        /// 女神枪手
        /// </summary>
        AtGunner,
        /// <summary>
        /// 暗夜使者
        /// </summary>
        Thief,
        /// <summary>
        /// 男格斗家
        /// </summary>
        AtFighter,
        /// <summary>
        /// 男魔法师
        /// </summary>
        AtMage,
        /// <summary>
        /// 暗黑武士
        /// </summary>
        DemonicSwordman,
        /// <summary>
        /// 缔造者
        /// </summary>
        CreatorMage,
        /// <summary>
        /// 女鬼剑士
        /// </summary>
        AtSwordman,
        /// <summary>
        /// 女圣职者
        /// </summary>
        AtPriest,
        Placement13,
        Placement14,
        Placement15,
        Placement16,
        Placement17,
        Placement18,
        Placement19,
        Placement20,
        Placement21,
        Placement22,
        Placement23,
        Placement24,
        Placement25,
        Placement26,
        Placement27,
        Placement28,
        Placement29,
        Placement30,
        Placement31
    }

}
