﻿using Pvf.Core;

namespace Pvf.Runtime.Common
{
    public enum AttributeDefine
    {
        /// <summary>
        /// 物理攻击力
        /// </summary>
        [PvfDynamicElementMap(typeof(RangedAttachValue))]
        EquipmentPhysicalAttack,
        /// <summary>
        /// 魔法攻击力
        /// </summary>
        [PvfDynamicElementMap(typeof(RangedAttachValue))]
        EquipmentMagicalAttack,
        /// <summary>
        /// 独立攻击力
        /// </summary>
        SeparateAttack,
        /// <summary>
        /// 物理防御力
        /// </summary>
        [PvfDynamicElementMap(typeof(RangedAttachValue))]
        EquipmentPhysicalDefense,
        /// <summary>
        /// 魔法防御力
        /// </summary>
        [PvfDynamicElementMap(typeof(RangedAttachValue))]
        EquipmentMagicalDefense,
        /// <summary>
        /// 力量
        /// </summary>
        PhysicalAttack,
        /// <summary>
        /// 智力
        /// </summary>
        MagicalAttack,
        /// <summary>
        /// 体力
        /// </summary>
        PhysicalDefense,
        /// <summary>
        /// 精神
        /// </summary>
        MagicalDefense,
        /// <summary>
        /// 抗魔值
        /// </summary>
        AntiEvil,
        /// <summary>
        /// 攻击速度
        /// </summary>
        AttackSpeed,
        /// <summary>
        /// 施放速度
        /// </summary>
        CastSpeed,
        /// <summary>
        /// 移动速度
        /// </summary>
        MoveSpeed,
        /// <summary>
        /// 硬直
        /// </summary>
        HitRecovery,
        /// <summary>
        /// 物理暴击率
        /// </summary>
        PhysicalCriticalHit,
        /// <summary>
        /// 魔法暴击率
        /// </summary>
        MagicalCriticalHit,
        /// <summary>
        /// 命中率（取相反数）
        /// </summary>
        Stuck,
        /// <summary>
        /// 回避率
        /// </summary>
        StuckResistance,
        /// <summary>
        /// 属性攻击
        /// </summary>
        [PvfIngore]
        ElementalProperty,
        /// <summary>
        /// 火属性强化
        /// </summary>
        FireAttack,
        /// <summary>
        /// 暗属性强化
        /// </summary>
        DarkAttack,
        /// <summary>
        /// 光属性强化
        /// </summary>
        LightAttack,
        /// <summary>
        /// 冰属性强化
        /// </summary>
        WaterAttack,
        /// <summary>
        /// 城镇移动速度
        /// </summary>
        RoomListMoveSpeedRate,
        /// <summary>
        /// 火属性抗性
        /// </summary>
        FireResistance,
        /// <summary>
        /// 暗属性抗性
        /// </summary>
        DarkResistance,
        /// <summary>
        /// 光属性抗性
        /// </summary>
        LightResistance,
        /// <summary>
        /// 冰属性抗性
        /// </summary>
        WaterResistance,
        /// <summary>
        /// 失明抗性
        /// </summary>
        BlindResistance,
        /// <summary>
        /// 感电抗性
        /// </summary>
        LightningResistance,
        /// <summary>
        /// 灼伤抗性
        /// </summary>
        BurnResistance,
        /// <summary>
        /// 冰冻抗性
        /// </summary>
        FreezeResistance,
        /// <summary>
        /// 束缚抗性
        /// </summary>
        HoldResistance,
        /// <summary>
        /// 睡眠抗性
        /// </summary>
        SleepResistance,
        /// <summary>
        /// 出血抗性
        /// </summary>
        BleedingResistance,
        /// <summary>
        /// 混乱抗性
        /// </summary>
        ConfuseResistance,
        /// <summary>
        /// 诅咒抗性
        /// </summary>
        CurseResistance,
        /// <summary>
        /// 石化抗性
        /// </summary>
        StoneResistance,
        /// <summary>
        /// HP最大值增加
        /// </summary>
        [PvfFields(false, "[HP MAX]")]
        HPMAX,
        /// <summary>
        /// MP最大值增加
        /// </summary>
        [PvfFields(false, "[MP MAX]")]
        MPMAX,
        /// <summary>
        /// HP恢复速度
        /// </summary>
        [PvfFields(false, "[HP regen speed]")]
        HPRegenSpeed,
        /// <summary>
        /// MP恢复速度
        /// </summary>
        [PvfFields(false, "[MP regen speed]")]
        MPRegenSpeed,
        /// <summary>
        /// 跳跃力
        /// </summary>
        JumpPower,
        /// <summary>
        /// 跳跃速度
        /// </summary>
        JumpSpeed,
        /// <summary>
        /// 重量
        /// </summary>
        [PvfIngore]
        Weight,
        /// <summary>
        /// 物品栏负重
        /// </summary>
        InventoryLimit,
        /// <summary>
        /// 生命值
        /// </summary>
        HP,
        /// <summary>
        /// 魔法值
        /// </summary>
        MP,
        /// <summary>
        /// 所有属性强化
        /// </summary>
        AllElementalAttack,
        /// <summary>
        /// 所有属性抗性
        /// </summary>
        AllElementalResistance,
        /// <summary>
        /// 所有异常抗性
        /// </summary>
        AllActivestatusResistance,
        /// <summary>
        /// 增加强化成功率
        /// </summary>
        UpgradeProbIncrease,
        /// <summary>
        /// 强化费用减少
        /// </summary>
        UpgradeCostDiscount,
        /// <summary>
        /// 物理伤害追加量减少
        /// </summary>
        PhysicalAbsoluteDefense,
        /// <summary>
        /// 魔法伤害追加量减少
        /// </summary>
        MagicalAbsoluteDefense,
        /// <summary>
        /// 任务道具掉落率
        /// </summary>
        QuestItemDropRateBonus,
        /// <summary>
        /// 毒抗
        /// </summary>
        PoisonResistance,
        /// <summary>
        /// 武器损坏抗性？
        /// </summary>
        WeaponBreakResistance,
        /// <summary>
        /// 穿刺抗性？
        /// </summary>
        PiercingResistance,
        /// <summary>
        /// 防具损坏抗性？
        /// </summary>
        ArmorBreakResistance,
        /// <summary>
        /// 眩晕抗性
        /// </summary>
        StunResistance,
        /// <summary>
        /// 减速抗性
        /// </summary>
        SlowResistance,
        /// <summary>
        /// 撞击眩晕抗性
        /// </summary>
        HitstunResistance,
        /// <summary>
        /// 
        /// </summary>
        [PvfFields(false, "[HP MAX rate]")]
        HPMAXRate,
        /// <summary>
        /// 
        /// </summary>
        [PvfFields(false, "[MP MAX rate]")]
        MPMAXRate
    }

}
