﻿using Pvf.Core;

namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 
    /// </summary>
    [PvfRequired]
    public sealed class PvfImage
    {
        public string Img { get; set; }

        public int Index { get; set; }

        public override string ToString()
        {
            return $"{Img}<{Index}>";
        }
    }

}
