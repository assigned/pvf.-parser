﻿using Pvf.Core;

namespace Pvf.Runtime.Common
{
    /// <summary>
    /// 附加类型
    /// </summary>
    public enum AttachType
    {
        /// <summary>
        /// 帐号绑定；
        /// </summary>
        Account,
        /// <summary>
        /// 无法交易
        /// </summary>
        [PvfFields(true, "[trade]]")]
        Trade,
        /// <summary>
        /// 无限制（可交易）
        /// </summary>
        Free,
        /// <summary>
        /// 封装
        /// </summary>
        Sealing,
        /// <summary>
        /// 无法删除
        /// </summary>
        TradeDelete,
        /// <summary>
        /// 封装 只能交易一次
        /// </summary>
        SealingTrade
    }

}
