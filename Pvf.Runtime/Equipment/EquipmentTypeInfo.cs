﻿using Pvf.Core;

namespace Pvf.Runtime.Equipment
{
    /// <summary>
    /// 装备类型信息
    /// </summary>
    [PvfRequired]
    public sealed class EquipmentTypeInfo
    {
        /// <summary>
        /// 装备类型
        /// </summary>
        public EquipmentType EquipmentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PriceTableIndex { get; set; }

        public override string ToString()
        {
            return $"{EquipmentType}<{PriceTableIndex}>";
        }
    }

}
