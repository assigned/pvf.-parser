﻿using Pvf.Core;

namespace Pvf.Runtime.Equipment
{
    /// <summary>
    /// 装备类型
    /// </summary>
    public enum EquipmentType
    {
        None,
        /// <summary>
        /// 武器
        /// </summary>
        Weapon,
        /// <summary>
        /// 项链
        /// </summary>
        Amulet,
        /// <summary>
        /// 手镯
        /// </summary>
        Wrist,
        /// <summary>
        /// 戒指
        /// </summary>
        Ring,
        /// <summary>
        /// 上衣
        /// </summary>
        Coat,
        /// <summary>
        /// 护肩
        /// </summary>
        Shoulder,
        /// <summary>
        /// 腰带
        /// </summary>
        Waist,
        /// <summary>
        /// 下装
        /// </summary>
        Pants,
        /// <summary>
        /// 鞋子
        /// </summary>
        Shoes,
        /// <summary>
        /// 
        /// </summary>
        Placement0,
        /// <summary>
        /// 称号
        /// </summary>
        TitleName,
        /// <summary>
        /// 辅助装备
        /// </summary>
        Support,
        /// <summary>
        /// 魔法石
        /// </summary>
        MagicStone,
        /// <summary>
        /// 帽子-时装
        /// </summary>
        HatAvatar,
        /// <summary>
        /// 头部-时装
        /// </summary>
        HairAvatar,
        /// <summary>
        /// 脸部
        /// </summary>
        FaceAvatar,
        /// <summary>
        /// 胸部
        /// </summary>
        BreastAvatar,
        /// <summary>
        /// 上衣
        /// </summary>
        CoatAvatar,
        /// <summary>
        /// 下装
        /// </summary>
        PantsAvatar,
        /// <summary>
        /// 腰部
        /// </summary>
        [PvfFields(true, "[belt avatar]")]
        WaistAvatar,
        /// <summary>
        /// 鞋子
        /// </summary>
        ShoesAvatar,
        /// <summary>
        /// 皮肤
        /// </summary>
        SkinAvatar,
        /// <summary>
        /// 光环
        /// </summary>
        AuroraAvatar,
        /// <summary>
        /// 武器装扮
        /// </summary>
        WeaponAvatar,
        /// <summary>
        /// 宠物装备（蓝）
        /// </summary>
        ArtifactBlue,
        /// <summary>
        /// 宠物装备（绿）
        /// </summary>
        ArtifactGreen,
        /// <summary>
        /// 宠物装备（红）
        /// </summary>
        ArtifactRed,
        /// <summary>
        /// 宠物
        /// </summary>
        Creature
    }

}
