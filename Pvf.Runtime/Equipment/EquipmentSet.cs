﻿using Pvf.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pvf.Runtime.Equipment
{
    public interface IEquipmentSet
    {
        
    }
    
    /// <summary>
    /// 套装
    /// </summary>
    public class EquipmentSet : PvfObject, IEquipmentSet
    {
        /// <summary>
        /// 套装名称
        /// </summary>
        public string SetName { get; set; }
        
        /// <summary>
        /// 套装装备
        /// </summary>
        [PvfIngore]
        public List<Equipment> SetItems { get; set; }
    }
}
