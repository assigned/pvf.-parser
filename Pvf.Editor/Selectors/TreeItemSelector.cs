﻿using Pvf.Editor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Pvf.Editor.Selectors
{
    public class TreeItemSelector : DataTemplateSelector
    {
        public DataTemplate FileTemplate { get; set; }

        public DataTemplate FolderTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if(item.GetType() == typeof(FileItemModel))
            {
                return FileTemplate;
            }
            else if(item.GetType() == typeof(FileContainerModel))
            {
                return FolderTemplate;
            }
            return base.SelectTemplate(item, container);
        }
    }
}
