﻿using Pvf.Core;
using Pvf.Editor.Extensions;
using Pvf.Editor.Models;
using Pvf.Editor.ViewModels;
using System;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Pvf.Editor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IPvfEnv _env;

        public MainWindow()
        {
            InitializeComponent();

            Closed += MainWindow_Closed;
            //注册编码提供程序
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            if (_env != null)
            {
                _env.Dispose();
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is not MainWindowViewModel viewModel)
            {
                return;
            }

            if (_env != null)
            {
                return;
            }

            _env = await PvfEnvFactory.Create(@"f:\DOFData\Script.pvf", logger: new DefaultConsoleLogger(), lang: "zh-cn", nlang: "kor");

            if(_env == null)
            {
                return;
            }

            viewModel.FileExplorer = _env.FileExplorer;

            var folders = _env.FileExplorer.GetTopFileContainers();

            var assets = viewModel.FileContainers[0];

            foreach (var folder in folders)
            {
                assets.Items.Add(ModelExtensions.CreateFileContainer(folder.Name, folder));
            }
        }
    }
}
